(function($){
  jQuery(document).ready(function($){
    $("head").append (`<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/le-frog/jquery-ui.min.css" rel="stylesheet" type="text/css">`);
    
    $("body").append(`<style>${GM_getResourceText("styling")}</style>
    <div id="matchingContainer"><input id="matchingText" type="text" value="Katsumi"><div id="matchingResult"></div></div>`);
    matchingString = "";
    $("#matchingText").on("keydown", function(e){
      var et = $(e.target);
      if(e.which == 13){
        //e.preventDefault();
        matchingString = et.val();
      }
    });
    
    
    /* //TODO: Använd mutationObserver istället för setInterval..
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    var list = $('#YPCqFe').parent().get(0);
    
    var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
    if (mutation.type == 'childList') {
    //console.log( mutation.addedNodes );
    if( mutation.addedNodes.length == 5){
    $('#productGrid').trigger("calendarUpdated");
  }
}
});
});

observer.observe(list, {
childList: true,
characterData: true,
subtree: true,
});

$('#YPCqFe').on("calendarUpdated", function(){
});
*/

setInterval(function(){
  var counter = 0;
  
  $('div.gVNoLb').each(function(i,e){
    //$('div.gVNoLb').mouseenter(function(e){
    $et = $(e);
    $btnParent = $et.parent().parent();
    var bg = $btnParent.css('background-color');
    if( bg != 'rgb(182, 217, 199)' ) return;
    
    
    eventtitle = $et.parent().find("span.FAxxKc").html();
    
    var obj = {};
    let html = $et.text();
    //let regexr = /([0-9]{2}:[0-9]{2})-([0-9]{2}:[0-9]{2})/g;
    //let match = html.match(regexr);
    var x = "";
    if(html.includes("–"))
    x = html.split("–");
    else if(html.includes("-"))
    x = html.split("-");
    else
    return false;
    
    obj.x = x;
    
    let xF = x[0].split(":");
    let xFh = parseInt(xF[0]);
    let xFm = parseInt(xF[1]);
    let xFd = new Date();
    xFd.setHours(xFh);
    xFd.setMinutes(xFm);
    
    obj.fromDate = xFd;
    
    let xT = x[1].split(":");
    let xTh = parseInt(xT[0]);
    let xTm = parseInt(xT[1]);
    let xTd = new Date();
    xTd.setHours(xTh);
    xTd.setMinutes(xTm);
    
    obj.toDate = xTd;
    
    var diff = Math.abs(xTd - xFd);
    obj.diff = diff;
    var hours = Math.floor(diff/1000/60/60);
    var minutes = Math.floor((diff/1000)/60);
    var minutes_remaining = minutes%60;
    
    var short = hm(obj);
    
    if( !$et.find(".short").length ){ $et.append(`<div class="short"></div>`); }
    $et.find(".short").html(` (${short}) `);
    
    if(matchingString !== "" && eventtitle.toLowerCase().includes(matchingString.toLowerCase())){
      counter += diff;
      obj.diff = counter;
      $("#matchingResult").html( `Sum o/ ${eventtitle}, ${hm(obj)} ` ); // console.log( hm(counter) );
    }
    
    //});
  });
},1000);
});
function hm(obj){  // TODO: function (int)90 => 1h 30m
  var diff = obj.diff;
  var x = obj.x;
  var hours = Math.floor(diff/1000/60/60);
  var minutes = Math.floor((diff/1000)/60);
  var minutes_remaining = minutes%60;
  
  if(minutes_remaining != 0){
    if(hours > 0){
      long = `Time between ${x[0]} and ${x[1]} is ${hours}h ${minutes_remaining}m.`;
      short = `${hours}h${minutes_remaining}m`;
    } else {
      long = `Time between ${x[0]} and ${x[1]} is ${minutes_remaining}m.`;
      short = `${minutes_remaining}m`;
    }
  } else {
    long = `Time between ${x[0]} and ${x[1]} is ${hours}h.`;
    short = `${hours}h`;
  }
  
  if(obj.getLong)
  return long;
  else
  return short;
}
})(jQuery);