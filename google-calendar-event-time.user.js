// ==UserScript==

// @name         Google Calendar Event Time
// @version      1.0.0
// @description  Räknar kontinuerligt ut hur länge eventen på kalendern varar och låter en slå samman tiden av event med valt namn.
// @author       Anton

// @downloadURL https://bitbucket.org/antonolsson91/google-calendar-event-time/raw/master/google-calendar-event-time.user.js
// @updateURL   https://bitbucket.org/antonolsson91/google-calendar-event-time/raw/master/google-calendar-event-time.user.js
// @resource    styling css/main.css
// @require     js/main.js

// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js
// @require      https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js

// @match        https://calendar.google.com/calendar/*
// @grant       GM_getResourceText
// @grant       GM_addStyle

// ==/UserScript==